/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  View,
  ActivityIndicator,
  SafeAreaView,
  Button
} from 'react-native';

import AdMob from './src/firebase/addMob';
import HomeScreen from './src/screens/HomeScreen';

export default class App extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      loading : true,
      showHome : false
    }
  }

  componentDidMount(){
    setTimeout(()=>{
      this.setState({loading : false})
    },2000)

    setTimeout(()=>{
      this.setState({ showHome: true });
    },10000)
  }
  render(){
    console.log('====================================');
    console.log(this.state.showHome);
    console.log('====================================');

      return(
        <SafeAreaView>
          {/* <AdMob /> */}
          {
            this.state.showHome ?
            <HomeScreen /> : <AdMob />
          }
         
        </SafeAreaView>
        
      )
  }
}