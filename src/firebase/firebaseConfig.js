//import * as firebase from 'firebase';
import firebase from '@react-native-firebase/app';

const firebaseConfig = {
    apiKey: "AIzaSyCV32mK13hQqH5g2Wu-x8Qp953g3l_S0QU",
    authDomain: "social-login-and-admob.firebaseapp.com",
    databaseURL: "https://social-login-and-admob.firebaseio.com",
    projectId: "social-login-and-admob",
    storageBucket: "social-login-and-admob.appspot.com",
    messagingSenderId: "1086080530057",
    appId: "1:1086080530057:web:6971767ce8205ea8f0f858"
};

if (!firebase.apps.length) {
   // firebase.initializeApp({});
    firebase.initializeApp(firebaseConfig);
}


export default firebase;