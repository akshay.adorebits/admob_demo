import React from 'react';
import { Button, View } from 'react-native';

import firebase from '../firebase/firebaseConfig';
import admob, { MaxAdContentRating,RewardedAdEventType,BannerAdSize, RewardedAd, BannerAd, TestIds } from '@react-native-firebase/admob';

const adUnitId = __DEV__ ? TestIds.REWARDED : 'ca-app-pub-4418545612149878~9455975621';

const rewarded = RewardedAd.createForAdRequest(adUnitId, {
    requestNonPersonalizedAdsOnly: true,
    keywords: ['fashion', 'clothing'],
  });

export default class AddMob extends React.Component{
  state = {
    loaded : false
  }

  componentDidMount(){
    // GoogleSignin.configure({
    //   offlineAccess: true,
    //   webClientId: "1086080530057-288uvi6lqmqrg618e6okp9akoqskrk7b.apps.googleusercontent.com"
    // })

    // admob banner

    // admob()
    // .setRequestConfiguration({
    //   // Update all future requests suitable for parental guidance
    //   maxAdContentRating: MaxAdContentRating.PG,

    //   // Indicates that you want your content treated as child-directed for purposes of COPPA.
    //   tagForChildDirectedTreatment: true,

    //   // Indicates that you want the ad request to be handled in a
    //   // manner suitable for users under the age of consent.
    //   tagForUnderAgeOfConsent: true,
    // })
    // .then(() => {
    //   // Request config successfully set!
    // });

    // end admob banner

    

    const eventListener = rewarded.onAdEvent((type, error, reward) => {
      if (type === RewardedAdEventType.LOADED) {
        this.setState({loaded : true});
        rewarded.show();
      }

      if (type === RewardedAdEventType.EARNED_REWARD) {
        console.log('User earned reward of ', reward);
      }
    });

    // Start loading the rewarded ad straight away
    rewarded.load();

    // Unsubscribe from events on unmount
    return () => {
      eventListener();
    };

  }
  render(){
    // if (this.state.loaded) {
    //   //return (() => {
    //     rewarded.show();
    //   //})
    // }
    return(
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
       
        {/* <BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER}/> */}
      </View>
    )
  }
}