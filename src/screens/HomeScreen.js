import React,{ Component } from 'react';
import {StyleSheet, View, Text, FlatList,TouchableOpacity,Image} from 'react-native';
import userList from '../firebase/UserData';
import firebase from '../firebase/firebaseConfig';
import admob, { MaxAdContentRating,RewardedAdEventType,BannerAdSize, RewardedAd, BannerAd, TestIds } from '@react-native-firebase/admob';

//const adUnitId = __DEV__ ? TestIds.BANNER : 'ca-app-pub-xxxxxxxxxxxxx/yyyyyyyyyyyyyy';

export default class HomeScreen extends Component{
    state = {
        datasource : '',
        loading : false
    }
   
    componentDidMount(){
        // admob banner

        admob()
        .setRequestConfiguration({
          // Update all future requests suitable for parental guidance
          maxAdContentRating: MaxAdContentRating.PG,

          // Indicates that you want your content treated as child-directed for purposes of COPPA.
          tagForChildDirectedTreatment: true,

          // Indicates that you want the ad request to be handled in a
          // manner suitable for users under the age of consent.
          tagForUnderAgeOfConsent: true,
        })
        .then(() => {
          // Request config successfully set!
        });

        // end admob banner
        this.apiCall();
    }

    apiCall(){
        this.setState({loading : true})
        fetch("https://jsonplaceholder.typicode.com/posts",{
            method : 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
        .then((response)=>response.json())
        .then((responseJson)=>{
    
           this.setAd(responseJson);
        })
        .catch((err)=>{
            console.log("error ==>",err)
        })
    }
    
    setAd(response){
        
        const  numColumns = 1;
        const addBannerIndex = 5;
        const arr = [];
        var tmp = [];
        
        response.forEach((val, index) => {
        if (index % numColumns == 0 && index != 0){
            arr.push(tmp);
            tmp = [];
        }
        if (index % addBannerIndex == 0 && index != 0){
            arr.push([{type: 'banner'}]);
            tmp = [];
        }
        tmp.push(val);
        
        });
        arr.push(tmp);
       
        this.setState({ datasource : arr})
        //return arr; 
    }

    _renderItem = (({item,index}) =>{
       
        if (item[0].type == "banner"){
            return (
                <View style={{flex:1,alignItems:'center'}}>
                    {/* <BannerAd unitId={TestIds.BANNER} size={BannerAdSize.BANNER}/> */}
                    <BannerAd
                        unitId={TestIds.BANNER}
                        size={BannerAdSize.BANNER}
                        requestOptions={{
                            requestNonPersonalizedAdsOnly: true,
                        }}
                        />
                </View>
            )
        }
        return (
            <View key={index} style={{ flexDirection: 'row', backgroundColor: '#fff', marginBottom: 2, height: 90,borderBottomWidth :1 }}>
          
                <View style={{ flexDirection: 'row', flexGrow: 1, flexShrink: 1, alignSelf: 'center',paddingLeft:10, }}>
                    <View style={{ flexGrow: 1, flexShrink: 1, alignSelf: 'center' }}>
                        <View style={{flexDirection:'row',marginBottom : 8}}>
                            <Text numberOfLines={1} style={{ fontSize: 15,fontWeight:"600" }}>{item[0].title}</Text>
                            
                        </View>
                        
                        <Text numberOfLines={1} style={{ color: '#333333', marginBottom: 10 }}>{item[0].body}</Text>
                        
                    </View>
                </View>
            </View>
        )
    })

    _renderFooter = () => {
        return (
          //Footer View with Load More button
          <View style={styles.footer}>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={getData}
              //On Click of button load more data
              style={styles.loadMoreBtn}>
              <Text style={styles.btnText}>Load More</Text>
              
            </TouchableOpacity>
          </View>
        );
    };
    
    render(){     
        return(
            <View>
                <View style={{height:50,borderBottomWidth : 1, borderBottomColor :'#fff'}}>
                    <Text style={{fontSize:20,fontWeight:"700",marginHorizontal:20,marginTop:10}}>HomeScreen</Text>
                </View>
                <FlatList 
                    data={this.state.datasource}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this._renderItem}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex:1,
       // justifyContent: 'center',
       // alignItems:'center'
    }
})